package com.petmatch.service;

import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;
import com.petmatch.model.core.User;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class MongoConnectionManager {

	private Datastore datastore;
	private MongoClient client;
	
	@PostConstruct
	private  void init(){
		client = new MongoClient("localhost", 27017);
		Morphia m = new Morphia();
		m.map(User.class);
		this.datastore = m.createDatastore(client, "petmatch_db");
	}
	
	@Lock(LockType.READ)
	public Datastore getDatastore() {
		return this.datastore;
	}
	
	@Lock(LockType.READ)
	public MongoClient getMongoClient(){
		return this.client;
	}	
}