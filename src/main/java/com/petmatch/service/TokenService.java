package com.petmatch.service;

import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.petmatch.dao.TokenDAO;
import com.petmatch.model.util.Token;

@Stateless
public class TokenService {
	@Inject	private TokenDAO tokenDAO;
	@Inject private MongoConnectionManager connManager;
	
	private MongoClient client;
	private MongoDatabase database;
	private MongoCollection<Document> collectionTokens;
	
	public enum TokenStatus{
		SUCCESS,NOTOKEN
	}
	
	@PostConstruct
	public void init(){
		client = connManager.getDatastore().getMongo();
		database = client.getDatabase("petmatch_db");
		collectionTokens = database.getCollection("tokens");
	}
	
	public TokenStatus checkToken(String token) {
		
		Token tkn = tokenDAO.findByTokenKey(token);
				
		if(tkn == null){
			return TokenStatus.NOTOKEN;
		}

		return TokenStatus.SUCCESS;
	}
	
	public boolean userOnline(String eMail){
		
		if(tokenDAO.findByEmail(eMail) == null)
			return false;
		
		return true;
	}
	
	public String generateToken(String eMail){
		String keychars = "abcdefghijklmnoprstyvwzx123456789";
		int length = keychars.length();
		Random rnd = new Random();
		
		StringBuffer apikey = new StringBuffer();
		
		for (int i = 0; i <8; i++) {
			char rndChar = keychars.charAt(rnd.nextInt(length));
			apikey.append(rndChar);
		}
		
		String tokenKey = apikey.toString();
		
		Token tokenToSave = new Token();
		tokenToSave.setUserEmail(eMail);
		tokenToSave.setTokenKey(tokenKey);
		
		try { 
			tokenDAO.save(tokenToSave);
		} catch (Exception e) {
			throw e;
		}
			
			return tokenKey;
	}
}
