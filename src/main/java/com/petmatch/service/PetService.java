package com.petmatch.service;

import java.util.ArrayList;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.petmatch.dao.PetDAO;
import com.petmatch.model.core.Pet;

@Stateless
public class PetService {
	@Inject private PetDAO petDAO;
	@Inject private UserService usrSrv;
	@Inject private MongoConnectionManager connManager;
	
	private MongoClient client;
	private MongoDatabase database;
	private MongoCollection<Document> collectionPets;
	
	@PostConstruct
	public void init(){
		client = connManager.getDatastore().getMongo();
		database = client.getDatabase("petmatch_db");
		collectionPets = database.getCollection("pets");
	}
	
	public void addPet(String ownerEmail, String petName, String petType, String petGender,
			String petAge, String isSterilized, String petCity,
			String vetMail, String vetPhone) throws Exception{
		Pet p = new Pet();
		
		p.setOwnerEmail(ownerEmail);
		p.setPetName(petName);
		p.setPetType(petType);
		p.setPetGender(Integer.parseInt(petGender));
		p.setPetAge(Integer.parseInt(petAge));
		p.setSterilized(Boolean.parseBoolean(isSterilized));
		p.setPetCity(petCity);
		p.setPendingMatch(false);
		p.setPendingAdoption(false);
		p.setPrefType(new ArrayList<String>());
		p.setVetMail(vetMail);
		p.setVetPhone(vetPhone);
		p.setPetProfilePhotoPath(getRandomUrl(p.getPetType()));
		
		try {
			petDAO.save(p);
			usrSrv.addPetToUser(ownerEmail, p.getId().toString());
		} catch(Exception e) {
			throw e;
		}
	}
	
	public void updatePetWithPref(String petID, String petName, String petType, String petGender,
			String petAge, String isSterilized, String petCity,
			String prefType1, String prefType2, String prefType3,
			String pendingAdoption,	String vetMail, String vetPhone) throws Exception{
		Pet p = petDAO.findPetByID(petID);
		
		p.setPetName(petName);
		p.setPetType(petType);
		p.setPetGender(Integer.parseInt(petGender));
		p.setPetAge(Integer.parseInt(petAge));
		p.setSterilized(Boolean.parseBoolean(isSterilized));
		p.setPetCity(petCity);
		p.setPendingMatch(true);
		p.setPendingAdoption(Boolean.parseBoolean(pendingAdoption));
		
		ArrayList<String> fooList = p.getPrefType();
		if(fooList == null){
			fooList = new ArrayList<String>();
			fooList.add(prefType1);
			fooList.add(prefType2);
			fooList.add(prefType3);
			p.setPrefType(fooList);
		}
		else{
			fooList.set(0, prefType1);
			fooList.set(1, prefType2);
			fooList.set(2, prefType3);
		}
		
		p.setVetMail(vetMail);
		p.setVetPhone(vetPhone);
		
		try {
			petDAO.save(p);
		} catch(Exception e) {
			throw e;
		}
	}
	
	public void updatePetNoPref(String petID, String petName, String petType, String petGender,
			String petAge, String isSterilized, String petCity, String pendingAdoption,
			String vetMail, String vetPhone) throws Exception{
		Pet p = petDAO.findPetByID(petID);
		
		p.setPetName(petName);
		p.setPetType(petType);
		p.setPetGender(Integer.parseInt(petGender));
		p.setPetAge(Integer.parseInt(petAge));
		p.setSterilized(Boolean.parseBoolean(isSterilized));
		p.setPetCity(petCity);
		p.setPendingMatch(false);
		p.setPendingAdoption(Boolean.parseBoolean(pendingAdoption));
		p.setVetMail(vetMail);
		p.setVetPhone(vetPhone);
		
		try {
			petDAO.save(p);
		} catch(Exception e) {
			throw e;
		}
	}
	
	public Pet getPetFull(String petID) throws Exception {
		return petDAO.findPetByID(petID);
	}
	
	public boolean petExists(String petID) throws Exception {
		Pet pet = petDAO.findPetByID(petID);
		
		if(pet == null)
			return false;
		
		return true;
	}
	
	public void updatePetInDB(Pet p) throws Exception {
		try {
			petDAO.save(p);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public String getRandomUrl(String type){
		ArrayList<String> urlList = new ArrayList<String>();
		
		if(type.equals("bulldog")){
			urlList.add("https://pixabay.com/get/ec36b90b20f11c2ad65a5854e34f4f94e773e4c818b5184796f7c671a0e4_640.jpg");
			urlList.add("https://pixabay.com/get/e131b50c20e90825d0471400e5454492e771ffd41db8174292f7c97eae_640.jpg");
			urlList.add("https://pixabay.com/get/e837b30b2af2063ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e835b5082df5093ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/eb36b00d2cf5063ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/ef35b00829f01c2ad65a5854e34f4f94e773e4c818b5184796f7c671a0e4_640.jpg");
			urlList.add("https://pixabay.com/get/ea3db5092cf31c2ad65a5854e34f4f94e773e4c818b5184796f7c671a0e4_640.jpg");
			urlList.add("https://pixabay.com/get/e133b80f2bf61c2ad65a5854e34f4f94e773e4c818b5184796f7c671a0e4_640.jpg");
			urlList.add("https://pixabay.com/get/e837b50928f6083ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e83db6062af0053ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e83db90a2af4033ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b50b29f5013ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b20f2df3043ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e83db50a2af5063ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e83db40c2cf7093ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b90829f5023ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e837b5092efc083ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e83cb40e2af6073ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/eb35b80c21f0043ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
			urlList.add("https://pixabay.com/get/e83db90a2af4043ed95c4518b749459fe173e6d304b0154893f3c67eafeabc_640.jpg");
		}
		
		else if(type.equals("golden") || type.equals("golden retriever")){
			urlList.add("https://pixabay.com/get/e837b10820f5073ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b10c29fd043ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b00b2af0003ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b10c2af5033ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b50c2efc033ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b50c2efc053ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/e83db20620f4093ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b50c2efc073ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb36b10e21f7043ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b90721f7023ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b40c2ef5043ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b10c29fd063ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/e83cb80b2efd033ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/e837b90b28f6063ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/e837b90c21f6053ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb35b8062ffc023ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb35b8062ffc003ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b9072df7093ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/e83db2062af5073ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b40c2ef5003ed95c4518b749459fe173e6d304b0154893f3c670a0edbc_640.jpg");
		}
		
		else if(type.equals("rottweiler")){
			urlList.add("https://pixabay.com/get/eb34b70f2bf4093ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b70f2bf4063ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b70f2bf5013ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e83cb70b28f4023ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e832b50f2bf0033ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e830b6092cfd063ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e830b20a21f1013ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e832b60e20f3093ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e832b90a2ff2013ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e830b70e28f5003ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e830b50a2cf1073ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e830b0082cf2073ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/ed30b40d2af11c2ad65a5854e34f4f94e773e4c818b5184796f7c870a4ed_640.jpg");
			urlList.add("https://pixabay.com/get/e835b4062cfd053ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e830b0082cf2043ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
			urlList.add("https://pixabay.com/get/e137b2082af31c2ad65a5854e34f4f94e773e4c818b5184796f7c870a4ed_640.jpg");
			urlList.add("https://pixabay.com/get/ee30b10a2df41c2ad65a5854e34f4f94e773e4c818b5184796f7c870a4ed_640.jpg");
			urlList.add("https://pixabay.com/get/ef3db50720f41c2ad65a5854e34f4f94e773e4c818b5184796f7c870a4ed_640.jpg");
			urlList.add("https://pixabay.com/get/e132b10e2bfd1c2ad65a5854e34f4f94e773e4c818b5184796f7c870a4ed_640.jpg");
			urlList.add("https://pixabay.com/get/e830b20a21f2003ed95c4518b749459fe173e6d304b0154893f3c670aeeeb5_640.jpg");
		}
		
		else if(type.equals("pekinese")){
			urlList.add("https://pixabay.com/get/e837b2062cf4093ed95c4518b749459fe173e6d304b0154893f3c87da3e5b2_640.jpg");
			urlList.add("https://pixabay.com/get/ea32b8072bf11c2ad65a5854e34f4f94e773e4c818b5184796f9c57dafea_640.jpg");
			urlList.add("https://pixabay.com/get/e036b60a21f11c2ad65a5854e34f4f94e773e4c818b5184796f9c57dafea_640.jpg");
			urlList.add("https://pixabay.com/get/ed36b10635fd0723cd0b4006ef4e4292e56ae3d110b7124891f4c97e_640.jpg");
			urlList.add("https://pixabay.com/get/e830b20d2dfc013ed95c4518b749459fe173e6d304b0154893f3c87da3e5b2_640.jpg");
			urlList.add("https://pixabay.com/get/ed3cb70e21fd1c2ad65a5854e34f4f94e773e4c818b5184796f9c57dafea_640.jpg");
			urlList.add("https://pixabay.com/get/e832b00620fd1c2ad65a5854e34f4f94e773e4c818b5184796f9c57dafea_640.jpg");			
		}
		
		else if(type.equals("labrador")){
			urlList.add("https://pixabay.com/get/eb37b80a2ef7053ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/e837b00f2df1083ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/e83db70e20f7083ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/e834b80f28f5043ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/e135b4072bfc1c2ad65a5854e34f4f94e773e4c818b5184796f9c57fa2ee_640.jpg");
			urlList.add("https://pixabay.com/get/e834b80b28fc023ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b3092ff4093ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/e83db20b2ff4033ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb37b80c20fd013ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b6072bf5053ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b90b2ff4083ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b10b29f3043ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/e83db2062cf0063ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b90d28f4043ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b90b2ffd003ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/e83db60c2af5093ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b9062bf6053ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b50b2df1003ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b30e2ff2053ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
			urlList.add("https://pixabay.com/get/eb35b50e2af5043ed95c4518b749459fe173e6d304b0154893f3c87da1e8b6_640.jpg");
		}
		
		else {
			urlList.add("https://pixabay.com/get/e832b50d2afd043ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
			urlList.add("https://pixabay.com/get/e837b00d2cf4013ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
			urlList.add("https://pixabay.com/get/e836b7082afd063ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
			urlList.add("https://pixabay.com/get/e837b30b2af2063ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
			urlList.add("https://pixabay.com/get/e835b9092afc073ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
			urlList.add("https://pixabay.com/get/e835b5082df5093ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
			urlList.add("https://pixabay.com/get/eb34b70d20f61c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/e83cb4072ff31c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/ea37b1072bf71c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/ee34b40a2cf11c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/eb3db10c2bf61c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/ef37b5062df51c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/e831b2082df71c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/e835b30f2ffd013ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
			urlList.add("https://pixabay.com/get/ea30b30c2af31c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/ef36b7072ff01c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/eb32b50e20f71c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/eb36b5072bf21c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/ed30b20f2ef71c2ad65a5854e34f4f94e773e4c818b5184796f8c178a2ec_640.jpg");
			urlList.add("https://pixabay.com/get/e832b3092cf4073ed95c4518b749459fe173e6d304b0154893f3c979a6e8b4_640.jpg");
		}
		
		/*
		urlList.add("https://pixabay.com/get/e134b90728fd1c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/ef37b6062cf71c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/e83cb10a2ef4093ed95c4518b749459fe173e6d304b0154893f3c670a3e9b3_640.jpg");
		urlList.add("https://pixabay.com/get/e835b50d2bf0083ed95c4518b749459fe173e6d304b0154893f3c670a3e9b3_640.jpg");
		urlList.add("https://pixabay.com/get/e835b4072ee90825d0471400e5454492e771ffd41db8174292f9c57da1_640.jpg");
		urlList.add("https://pixabay.com/get/e835b60829e90825d0471400e5454492e771ffd41db8174292f9c57da1_640.jpg");
		urlList.add("https://pixabay.com/get/e835b40d21e90825d0471400e5454492e771ffd41db8174292f9c57da1_640.jpg");
		urlList.add("https://pixabay.com/get/e835b50d2bf0073ed95c4518b749459fe173e6d304b0154893f3c670a3e9b3_640.jpg");
		urlList.add("https://pixabay.com/get/e835b6072fe90825d0471400e5454492e771ffd41db8174292f9c57da1_640.jpg");
		urlList.add("https://pixabay.com/get/e832b2092af61c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/e835b60a20e90825d0471400e5454492e771ffd41db8174292f9c57da1_640.jpg");
		urlList.add("https://pixabay.com/get/ed36b7082df01c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/ec36b6072bf61c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/eb36b9062bf51c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/e836b20c2ef0043ed95c4518b749459fe173e6d304b0154893f3c670a3e9b3_640.jpg");
		urlList.add("https://pixabay.com/get/eb34b70a21f5003ed95c4518b749459fe173e6d304b0154893f3c670a3e9b3_640.jpg");
		urlList.add("https://pixabay.com/get/e832b4092cf71c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/ef30b30f20f01c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/ee32b80821fd1c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		urlList.add("https://pixabay.com/get/ed35b80d2dfc1c2ad65a5854e34f4f94e773e4c818b5184796f7c87da3eb_640.jpg");
		*/
		
		Random rand = new Random();
		int iterator = rand.nextInt(20);
		
		return urlList.get(iterator);
	}
}
