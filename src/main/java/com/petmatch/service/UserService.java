package com.petmatch.service;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.bson.Document;
import org.mongodb.morphia.query.UpdateOperations;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.petmatch.dao.TokenDAO;
import com.petmatch.dao.UserDAO;
import com.petmatch.model.core.Preference;
import com.petmatch.model.core.User;
import com.petmatch.model.util.Token;


@Stateless
public class UserService {
	
	@Inject private UserDAO userDAO;
	@Inject private TokenDAO tokenDAO;
	@Inject private MongoConnectionManager connManager;
	private MongoClient client;
	private MongoDatabase database;
	private MongoCollection<Document> collectionUsers;
	
	@PostConstruct
	public void init(){
		client = connManager.getDatastore().getMongo();
		database = client.getDatabase("petmatch_db");
		collectionUsers = database.getCollection("users");
	}
	
	public boolean userExists(String email) throws Exception{		
		User usr = userDAO.findByEmail(email);
		
		if(usr == null)
			return false;
		
		return true;
	}
	
	public boolean passwordMatches(String email, String password) throws Exception{		
		User usr = userDAO.findByEmail(email);
		
		if(usr != null && usr.getPassword().equals(password))
			return true;
		
		return false;
	}
	
	public User getUser(String email) throws Exception{
		return userDAO.findByEmail(email);
	}
	
	public String getEmailFromToken(String tkn) throws Exception {
		return tokenDAO.findByTokenKey(tkn).getUserEmail(); 
	}
	
	public void register(String username, String ownerName, String ownerSurname,
			String eMail, String password, String phone,
			String city, String ownerGender){
		User u = new User();
		
		u.setUsername(username);
		u.setOwnerName(ownerName);
		u.setOwnerSurname(ownerSurname);
		u.setEmail(eMail);
		u.setPassword(password);
		u.setPhone(phone);
		u.setCity(city);
		u.setOwnerGender(Integer.parseInt(ownerGender));		
		u.setPremium(false);
		u.setPetIDList(new ArrayList<String>());
		
		try {
			userDAO.save(u);
		} catch(Exception e) {
			throw e;
		}
	}
	
	public void logout(String tokenKey) throws Exception{
		try {
			Token tkn = tokenDAO.findByTokenKey(tokenKey);
			tokenDAO.delete(tkn);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void addPetToUser(String eMail, String petID) throws Exception {
		try {
			User u = userDAO.findByEmail(eMail);
			ArrayList<String> fooList = u.getPetIDList();
		
			if(fooList == null)
				fooList = new ArrayList<String>();
		
			fooList.add(petID);
			u.setPetIDList(fooList);
		
			userDAO.save(u);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deletePetFromUser(String eMail, String petID) throws Exception {
		try {
			User u = userDAO.findByEmail(eMail);
			ArrayList<String> fooListID = u.getPetIDList();
				
			fooListID.remove(fooListID.indexOf(petID));
			u.setPetIDList(fooListID);
		
			userDAO.save(u);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public ArrayList<String> getUserPets(String eMail) throws Exception {
		ArrayList<String> fooList;
		
		try {
			User u = userDAO.findByEmail(eMail);
			fooList = u.getPetIDList();
			
			if(fooList == null)
				fooList = new ArrayList<String>();
		} catch (Exception e) {
			throw e;
		}
		
		return fooList;
	}
	
	public ArrayList<String> getMyAdopts(String eMail) throws Exception {
		ArrayList<String> fooList;
				
		try {
			User u = userDAO.findByEmail(eMail);
			fooList = u.getMyAdopts();
			
			if(fooList == null)
				fooList = new ArrayList<String>();
		} catch (Exception e) {
			throw e;
		}
		
		return fooList;
	}
	
	public void updateUser(User u) throws Exception {
		try {
			userDAO.save(u);
		} catch (Exception e) {
			throw e;
		}
	}
}