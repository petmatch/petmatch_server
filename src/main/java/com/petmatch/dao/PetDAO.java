package com.petmatch.dao;

import org.bson.types.ObjectId;

import com.petmatch.model.core.Pet;

public interface PetDAO extends BaseDAO<Pet,ObjectId>{
	public Pet findPetByPetName(String petName);
	public Pet findPetByID(String petID);
	public Pet findAdoptPet(String location, String eMail);
	public Pet findMatchPet(String type1, String type2, String type3, String eMail, int gender);
}
