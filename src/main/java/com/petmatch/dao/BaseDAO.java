package com.petmatch.dao;


import org.mongodb.morphia.dao.DAO;

public interface BaseDAO<T,K> extends DAO<T, K>{

	// We can add extra methods to be shared by all DAOs
	
}