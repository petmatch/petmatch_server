package com.petmatch.dao.impl;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import com.petmatch.dao.TokenDAO;
import com.petmatch.model.util.Token;

public class TokenDAOImpl extends BasicDAO<Token, ObjectId> implements TokenDAO {
	public TokenDAOImpl(Datastore ds){
		super(ds);
	}
	
	@Override
	public Token findByTokenKey(String tokenKey){
		return createQuery().field("tokenKey").equal(tokenKey).get();
	}
	
	@Override
	public Token findByEmail(String eMail){
		return createQuery().field("userEmail").equal(eMail).get();
	}
}
