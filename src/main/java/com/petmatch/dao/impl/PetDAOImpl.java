package com.petmatch.dao.impl;

import java.util.List;
import java.util.Random;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.FindOptions;
import org.mongodb.morphia.query.Query;

import com.petmatch.dao.PetDAO;
import com.petmatch.model.core.Pet;

public class PetDAOImpl extends BasicDAO<Pet, ObjectId> implements PetDAO{
	
	public PetDAOImpl(Datastore ds){
		super(ds);
	}
	
	@Override
	public Pet findPetByPetName(String petName){
		return createQuery().field("petName").equal(petName).get();
	}
	
	@Override
	public Pet findPetByID(String petID){
		ObjectId oid = new ObjectId(petID);
		return createQuery().field("_id").equal(oid).get();
	}
	
	@Override
	public Pet findAdoptPet(String location, String eMail){
		Query<Pet> q = createQuery().field("petCity").equal(location)
									.field("pendingAdoption").equal(true)
									.field("ownerEmail").notEqual(eMail);
		
		int count = (int) q.count();
		Random rand = new Random();
		int iterator = rand.nextInt(count);
		
		return q.get(new FindOptions().skip(iterator));
	}
	
	@Override
	public Pet findMatchPet(String type1, String type2, String type3, String eMail, int gender){
		Query<Pet> q = createQuery();
		q.and(q.and(new Criteria[]{createQuery().criteria("pendingMatch").equal(true),
								   createQuery().criteria("ownerEmail").notEqual(eMail),
								   createQuery().criteria("petGender").notEqual(gender)}),
			  q.or(new Criteria[]{createQuery().criteria("petType").equal(type1),
					  		  	  createQuery().criteria("petType").equal(type2),
					  			  createQuery().criteria("petType").equal(type3)}
			));

		int count = (int) q.count();
		Random rand = new Random();
		int iterator = rand.nextInt(count);
		
		return q.get(new FindOptions().skip(iterator));
	}
	
}
