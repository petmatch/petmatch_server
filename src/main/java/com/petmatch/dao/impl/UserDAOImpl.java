package com.petmatch.dao.impl;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import com.petmatch.dao.UserDAO;
import com.petmatch.model.core.User;

public class UserDAOImpl extends BasicDAO<User, ObjectId> implements UserDAO{

	public UserDAOImpl(Datastore ds) {
		super(ds);
	}

	@Override
	public User findByEmail(String email) {
		return createQuery().field("eMail").equal(email).get();		
	}
}