package com.petmatch.dao.impl;


import javax.enterprise.inject.Produces;
import javax.inject.Inject;

import com.petmatch.dao.PetDAO;
import com.petmatch.dao.TokenDAO;
import com.petmatch.dao.UserDAO;
import com.petmatch.service.MongoConnectionManager;

/*Create request scoped data access objects for CDI injection and easy coding*/
public class DAOFactory {

	@Inject private MongoConnectionManager connManager;
	
	@Produces
	public UserDAO produceUserDAO(){
		UserDAO dao = new UserDAOImpl(connManager.getDatastore());	
		return dao;
	}
	
	@Produces
	public PetDAO producePetDAO(){
		PetDAO dao = new PetDAOImpl(connManager.getDatastore());	
		return dao;
	}
	
	@Produces
	public TokenDAO produceTokenDAO(){
		TokenDAO dao = new TokenDAOImpl(connManager.getDatastore());	
		return dao;
	}
}