package com.petmatch.dao;

import org.bson.types.ObjectId;
import com.petmatch.model.core.User;

public interface UserDAO extends BaseDAO<User,ObjectId>{
	public User findByEmail(String email);
}