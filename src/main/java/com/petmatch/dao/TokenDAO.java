package com.petmatch.dao;

import org.bson.types.ObjectId;

import com.petmatch.model.util.Token;

public interface TokenDAO extends BaseDAO<Token,ObjectId>{
	public Token findByTokenKey(String tokenKey);
	public Token findByEmail(String eMail);
}
