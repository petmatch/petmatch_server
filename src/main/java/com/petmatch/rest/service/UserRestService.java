package com.petmatch.rest.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.petmatch.dao.PetDAO;
import com.petmatch.model.core.Pet;
import com.petmatch.model.core.User;
import com.petmatch.service.TokenService;
import com.petmatch.service.UserService;

@Path("/users")
@RequestScoped
public class UserRestService {	
	
	@Inject
	private UserService usrSrv;
	@Inject
	private PetDAO petDAO;
	@Inject
	private TokenService tknSrv;
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(Map<String, String> user) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Login Request -----");
		
		try {
			if(usrSrv.userExists(user.get("eMail"))) {
				System.out.println("User exists.");
				if(usrSrv.passwordMatches(user.get("eMail"), user.get("password"))){
					System.out.println("Login Successful!");
					String tokenKey = tknSrv.generateToken(user.get("eMail"));
					User u = usrSrv.getUser(user.get("eMail"));
					
					responseObj.put("response", 1);
					responseObj.put("token", tokenKey);
					responseObj.put("username", u.getUsername());
					responseObj.put("ownerName", u.getOwnerName());
					responseObj.put("ownerSurname", u.getOwnerSurname());
					responseObj.put("eMail", u.getEmail());
					responseObj.put("phone", u.getPhone());
					responseObj.put("city", u.getCity());
					responseObj.put("ownerGender", u.getOwnerGender());
					responseObj.put("isPremium", u.isPremium());
				}
				else {
					System.out.println("Login Failed: Password is incorrect!");
					responseObj.put("response", 0);
				}
			}
			
			else {
				System.out.println("Login Failed: User does not exist!");
				responseObj.put("response", -1);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(Map<String, String> user) throws Exception {
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		
		System.out.println("----- Register Request -----");
		
		if(usrSrv.userExists(user.get("eMail"))) {
			responseObj.put("response", -1);
			System.out.println("Register failed: User exists.");
		}
		
		else {
			try {
				usrSrv.register(
						user.get("username"),
						user.get("ownerName"),
						user.get("ownerSurname"),
						user.get("eMail"),
						user.get("password"),
						user.get("phone"),
						user.get("city"),
						user.get("ownerGender"));
				responseObj.put("response", 1);
				String tokenKey = tknSrv.generateToken(user.get("eMail"));
				responseObj.put("token", tokenKey);
				System.out.println("Register successful.");
			} catch (Exception e) {
				e.printStackTrace();
				responseObj.put("response", 0);
				System.out.println("Register failed.");
				throw e;
			}
			
		}
	
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}

	@POST
	@Path("/checkUser")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkUser(Map<String, String> user){
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Check User -----");
		/* add token check here*/
		boolean ue = false;			// user exists
		
		try {
			ue = usrSrv.userExists(user.get("eMail"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(ue){
			responseObj.put("response", 1);
		}
		else {
			responseObj.put("response", 0);
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response logout(Map<String, String> tkn) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Logout Request -----");
		/* add token check here*/
		try {
			usrSrv.logout(tkn.get("token"));
			responseObj.put("response", 1);
			System.out.println("Logout Succesful.");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Logout Failed.");
			responseObj.put("response", 0);
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build(); 
	}
	
	@POST
	@Path("/getUserPets")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserPets(Map<String, String> tkn){
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Get User Pets Request ----- ");
		/* add token check here*/
		try {
			String eMail = usrSrv.getEmailFromToken(tkn.get("token"));
			ArrayList<String> list = usrSrv.getUserPets(eMail);
			
			if(list == null)
				list = new ArrayList<String>();
			
			ArrayList<HashMap<String, Object>> listArray = new ArrayList<HashMap<String, Object>>();
						
			for(int i = 0; i < list.size(); i++) {
				HashMap<String, Object> temp = new HashMap<String, Object>();
				String tempPetID = list.get(i);
				temp.put("petID", tempPetID);
				temp.put("petProfilePhotoPath", petDAO.findPetByID(tempPetID).getPetProfilePhotoPath());
				temp.put("petName", petDAO.findPetByID(tempPetID).getPetName());
				listArray.add(temp);
			}
			
			responseObj.put("list", listArray);
			responseObj.put("response", 1);
			System.out.println("Get User Pets Successful.");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Get User Pets Failed.");
			responseObj.put("response", 0);
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/getMyAdopts")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMyAdopts(Map<String, String> tkn){
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Get MyAdopts Request ----- ");
		/* add token check here*/
		try {
			String eMail = usrSrv.getEmailFromToken(tkn.get("token"));
			ArrayList<String> list = usrSrv.getMyAdopts(eMail);
			
			responseObj.put("list", list);
			responseObj.put("response", 1);
			System.out.println("Get MyAdopts Successful.");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Get MyAdopts Failed.");
			responseObj.put("response", 0);
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	
	@POST
	@Path("/getMyMatches")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMyMatches(Map<String, String> tkn){
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Get MyMatches Request ----- ");
		/* add token check here */
		try {
			String eMail = usrSrv.getEmailFromToken(tkn.get("token"));
			User user = usrSrv.getUser(eMail);
			
			ArrayList<String> ad = user.getMyAdopts();
			if(ad == null)
				ad = new ArrayList<String>();
						
			ArrayList<HashMap<String, Object>> responseList = new ArrayList<HashMap<String, Object>>();

			for(int i = 0; i < ad.size(); i++) {
				HashMap<String, Object> temp = new HashMap<String, Object>();
				Pet p1 = petDAO.findPetByID(ad.get(i));
				
				temp.put("petID", ad.get(i));
				temp.put("petName", p1.getPetName());
				temp.put("petProfilePhotoPath", p1.getPetProfilePhotoPath());
				responseList.add(temp);
			}
			
			ArrayList<String> petList = user.getPetIDList();
			if(petList == null)
				petList = new ArrayList<String>();
				
			for(int j = 0; j < petList.size(); j++){
				ArrayList<String> myOut = petDAO.findPetByID(petList.get(j)).getOutgoingLikes();
				if(myOut == null)
					myOut = new ArrayList<String>();
				
				for(int k = 0; k < myOut.size(); k++){
					ArrayList<String> otherOut = petDAO.findPetByID(myOut.get(k)).getOutgoingLikes();
					if((otherOut != null) && (otherOut.contains(petList.get(j)))){
						HashMap<String, Object> temp = new HashMap<String, Object>();
						Pet p2 = petDAO.findPetByID(myOut.get(k));
						
						temp.put("petID", myOut.get(k));
						temp.put("petName", p2.getPetName());
						temp.put("petProfilePhotoPath", p2.getPetProfilePhotoPath());
						responseList.add(temp);
					}
				}
			}
			
			responseObj.put("list", responseList);
			responseObj.put("response", 1);
			System.out.println("Get MyMatches Successful.");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Get MyMatches Failed.");
			responseObj.put("response", 0);
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
}