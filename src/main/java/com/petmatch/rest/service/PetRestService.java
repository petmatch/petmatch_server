package com.petmatch.rest.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.petmatch.dao.PetDAO;
import com.petmatch.model.core.Pet;
import com.petmatch.model.core.User;
import com.petmatch.service.PetService;
import com.petmatch.service.TokenService;
import com.petmatch.service.UserService;
import com.petmatch.service.TokenService.TokenStatus;

@Path("/pets")
@RequestScoped
public class PetRestService {
	@Inject
	private PetService petSrv;
	@Inject
	private PetDAO petDAO;
	@Inject
	private UserService usrSrv;
	@Inject
	private TokenService tknSrv;
	
	@POST
	@Path("/addPet")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addPet(Map<String, String> pet) throws Exception {
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Add Pet Request ----- ");
		/* add token check here*/
		String eMail = usrSrv.getEmailFromToken(pet.get("token"));
		ArrayList<String> fooList = usrSrv.getUser(eMail).getPetIDList();
		
		int size = 0;
		
		if(fooList != null)
			size = fooList.size();
		
		if(size < 3){
			try {
				petSrv.addPet(eMail,
						pet.get("petName"),
						pet.get("petType"),
						pet.get("petGender"),
						pet.get("petAge"),
						pet.get("isSterilized"),
						pet.get("petCity"),
						pet.get("vetMail"),
						pet.get("vetPhone"));
								
				ArrayList<String> temp = usrSrv.getUser(eMail).getPetIDList();
								
				responseObj.put("petID", temp.get((temp.size()-1)));
				responseObj.put("response", 1);
				System.out.println("Pet is successfully added.");
			} catch (Exception e) {
				e.printStackTrace();
				responseObj.put("response", 0);
				System.out.println("Pet add failed.");
				throw e;
			}
		}
		
		else {
			responseObj.put("response", -1);
			System.out.println("Pet overflow.");
		}
		
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/deletePet")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletePet(Map<String, String> pet) throws Exception {
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Delete Pet Request ----- ");
		/* add token check here*/
		try {
			if(petSrv.petExists(pet.get("petID"))) {
				Pet p = petDAO.findPetByID(pet.get("petID"));
				String eMail = usrSrv.getEmailFromToken(pet.get("token"));
				if(p.getOwnerEmail().equals(eMail)) {
					petDAO.delete(p);
					usrSrv.deletePetFromUser(eMail, pet.get("petID"));
					
					responseObj.put("response", 1);
					System.out.println("Pet is successfully deleted.");
				}
				
				else {
					responseObj.put("response", -2);
					System.out.println("Pet belongs to someone else.");
				}
			}
			
			else {
				responseObj.put("response", -1);
				System.out.println("Pet does not exist.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Pet delete failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/getPetInfo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPetInfo(Map<String, String> pet){
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Get Pet Info Request ----- ");
		
		try {
			if(tknSrv.checkToken(pet.get("token")).equals(TokenStatus.SUCCESS)){
				if(petSrv.petExists(pet.get("petID"))){
					System.out.println("Pet exists.");
					Pet p = petSrv.getPetFull(pet.get("petID"));
				
					responseObj.put("petID", pet.get("petID"));
					responseObj.put("petName", p.getPetName());
					responseObj.put("petType", p.getPetType());
					responseObj.put("petGender", p.getPetGender());
					responseObj.put("petAge", p.getPetAge());
					responseObj.put("isSterilized", p.isSterilized());
					responseObj.put("petCity", p.getPetCity());
					responseObj.put("pendingAdoption", p.isPendingAdoption());
					responseObj.put("pendingMatch", p.isPendingMatch());
					responseObj.put("petProfilePhotoPath", p.getPetProfilePhotoPath());
					
					if(petSrv.getPetFull(pet.get("petID")).isPendingMatch()){
						responseObj.put("prefType1", p.getPrefType().get(0));
						responseObj.put("prefType2", p.getPrefType().get(1));
						responseObj.put("prefType3", p.getPrefType().get(2));
					}
					
					responseObj.put("vetMail", p.getVetMail());
					responseObj.put("vetPhone", p.getVetPhone());
					
					User u = usrSrv.getUser(p.getOwnerEmail());					
					responseObj.put("ownerName", u.getOwnerName());
					responseObj.put("ownerSurname", u.getOwnerSurname());
					responseObj.put("phone", u.getPhone());
					responseObj.put("city", u.getCity());
					responseObj.put("eMail", u.getEmail());
					
					responseObj.put("response", 1);
					
					System.out.println("Get Pet Info Succesful.");
				}
				
				else {
					responseObj.put("response", -1);
					System.out.println("Pet does not exist.");
				}
			}
			
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Get Pet Info Failed.");
		}

		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}	
	
	@POST
	@Path("/updatePet")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePet(Map<String, String> pet) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Update Pet Request ----- ");

		try {
			if(tknSrv.checkToken(pet.get("token")).equals(TokenStatus.SUCCESS)
					&& usrSrv.getEmailFromToken(pet.get("token")).equals(petSrv.getPetFull(pet.get("petID")).getOwnerEmail())){
				if(petSrv.petExists(pet.get("petID"))){
					if(Boolean.parseBoolean(pet.get("pendingMatch"))) {
						petSrv.updatePetWithPref(pet.get("petID"),
								pet.get("petName"),
								pet.get("petType"),
								pet.get("petGender"),
								pet.get("petAge"),
								pet.get("isSterilized"),
								pet.get("petCity"),
								pet.get("prefType1"),
								pet.get("prefType2"),
								pet.get("prefType3"),
								pet.get("pendingAdoption"),
								pet.get("vetMail"),
								pet.get("vetPhone"));
					}
					else {
						petSrv.updatePetNoPref(pet.get("petID"),
								pet.get("petName"),
								pet.get("petType"),
								pet.get("petGender"),
								pet.get("petAge"),
								pet.get("isSterilized"),
								pet.get("petCity"),
								pet.get("pendingAdoption"),
								pet.get("vetMail"),
								pet.get("vetPhone"));
					}

					responseObj.put("response", 1);
					System.out.println("Pet is successfully updated.");
				}
				else {
					responseObj.put("response", -1);
					System.out.println("Pet does not exist.");
				}
			}
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Update Pet Failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/findAdopt")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAdopt(Map<String, String> tkn) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Find Adopt Request ----- ");

		try {
			if(tknSrv.checkToken(tkn.get("token")).equals(TokenStatus.SUCCESS)){
				String eMail = usrSrv.getEmailFromToken(tkn.get("token"));
				String loc = usrSrv.getUser(eMail).getCity();
				Pet p = petDAO.findAdoptPet(loc, eMail);
				
				if(p == null){
					System.out.println("Pet Adopt not found.");
					responseObj.put("response", -1);
				}
				
				else {
					System.out.println("Pet Adopt found.");
					responseObj.put("petID", p.getId().toString());
					responseObj.put("petName", p.getPetName());
					responseObj.put("petType", p.getPetType());
					responseObj.put("petGender", p.getPetGender());
					responseObj.put("petAge", p.getPetAge());
					responseObj.put("isSterilized", p.isSterilized());
					responseObj.put("petCity", p.getPetCity());
					responseObj.put("vetMail", p.getVetMail());
					responseObj.put("vetPhone", p.getVetPhone());
					responseObj.put("petProfilePhotoPath", p.getPetProfilePhotoPath());
					
					User u = usrSrv.getUser(p.getOwnerEmail());					
					responseObj.put("ownerName", u.getOwnerName());
					responseObj.put("ownerSurame", u.getOwnerSurname());
					responseObj.put("phone", u.getPhone());
					responseObj.put("city", u.getCity());
					responseObj.put("eMail", u.getEmail());
					
					responseObj.put("response", 1);
				}
			}
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Find Adopt Failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/findMatch")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response findMatch(Map<String, String> pet) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Find Match Request ----- ");

		try {
			if(tknSrv.checkToken(pet.get("token")).equals(TokenStatus.SUCCESS)){
				if(petSrv.petExists(pet.get("petID"))){
					Pet requestPet = petDAO.findPetByID(pet.get("petID"));
					Pet responsePet = petDAO.findMatchPet(requestPet.getPrefType().get(0),
													      requestPet.getPrefType().get(1),
														  requestPet.getPrefType().get(2),
														  requestPet.getOwnerEmail(),
														  requestPet.getPetGender());
					
					if(responsePet == null){
						System.out.println("Pet Match not found.");
						responseObj.put("response", -3);
					}
					else{
						System.out.println("Pet Match found.");
						responseObj.put("petID", responsePet.getId().toString());
						responseObj.put("petName", responsePet.getPetName());
						responseObj.put("petType", responsePet.getPetType());
						responseObj.put("petGender", responsePet.getPetGender());
						responseObj.put("petAge", responsePet.getPetAge());
						responseObj.put("isSterilized", responsePet.isSterilized());
						responseObj.put("petCity", responsePet.getPetCity());
						responseObj.put("vetMail", responsePet.getVetMail());
						responseObj.put("vetPhone", responsePet.getVetPhone());
						responseObj.put("petProfilePhotoPath", responsePet.getPetProfilePhotoPath());
						
						User u = usrSrv.getUser(responsePet.getOwnerEmail());					
						responseObj.put("ownerName", u.getOwnerName());
						responseObj.put("ownerSurname", u.getOwnerSurname());
						responseObj.put("phone", u.getPhone());
						responseObj.put("city", u.getCity());
						responseObj.put("eMail", u.getEmail());
						
						responseObj.put("response", 1);
					}
				}
				else {
					responseObj.put("response", -1);
					System.out.println("Response pet does not exist.");
				}
			}
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Find Match Failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/likeAdopt")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response likeAdopt(Map<String, String> pet) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Like Adopt Request ----- ");

		try {
			if(tknSrv.checkToken(pet.get("token")).equals(TokenStatus.SUCCESS)){
				if(petSrv.petExists(pet.get("petID"))){
					String eMail = usrSrv.getEmailFromToken(pet.get("token"));
					User u = usrSrv.getUser(eMail);
					ArrayList<String> list = u.getMyAdopts();
					
					if(list == null)
						list = new ArrayList<String>();
										
					if(!list.contains(pet.get("petID"))){
						list.add(pet.get("petID"));
						u.setMyAdopts(list);
						usrSrv.updateUser(u);
					}
					
					responseObj.put("response", 1);
					System.out.println("Like Adopt Successful.");
				}
				else {
					responseObj.put("response", -1);
					System.out.println("Pet does not exist.");
				}
			}
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Like Adopt Failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/likeMatch")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response likeMatch(Map<String, String> pet) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Like Match Request ----- ");

		try {
			if(tknSrv.checkToken(pet.get("token")).equals(TokenStatus.SUCCESS)){
				if(petSrv.petExists(pet.get("myPetID")) && petSrv.petExists(pet.get("otherPetID"))){
					Pet myPet = petSrv.getPetFull(pet.get("myPetID"));
					Pet otherPet = petSrv.getPetFull(pet.get("otherPetID"));
					
					ArrayList<String> out = myPet.getOutgoingLikes();
				
					if(out == null)
						out = new ArrayList<String>();
					
					if(!out.contains(pet.get("otherPetID"))){
						out.add(pet.get("otherPetID"));
						myPet.setOutgoingLikes(out);
						petSrv.updatePetInDB(myPet);
					}
					
					ArrayList<String> in = otherPet.getIncomingLikes();
					
					if(in == null)
						in = new ArrayList<String>();
					
					if(!in.contains(pet.get("myPetID"))){
						in.add(pet.get("myPetID"));
						otherPet.setIncomingLikes(in);
						petSrv.updatePetInDB(otherPet);
					}
					
					responseObj.put("response", 1);
					System.out.println("Like Match successful.");
				}
				else {
					responseObj.put("response", -1);
					System.out.println("Pet does not exist.");
				}
			}
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Like Match Failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}

	@POST
	@Path("/getIncomingLikes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIncomingLikes(Map<String, String> pet) throws Exception {
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Get Incoming Likes Request ----- ");
		try {
			if(tknSrv.checkToken(pet.get("token")).equals(TokenStatus.SUCCESS)){
				if(petSrv.petExists(pet.get("petID"))){
					String eMail = usrSrv.getEmailFromToken(pet.get("token"));
					ArrayList<String> list = usrSrv.getUserPets(eMail);
					
					if(list == null)
						list = new ArrayList<String>();
					
					ArrayList<HashMap<String, Object>> listArray = new ArrayList<HashMap<String, Object>>();
								
					for(int i = 0; i < list.size(); i++) {
						HashMap<String, Object> temp = new HashMap<String, Object>();
						String tempPetID = list.get(i);
						
						ArrayList<String> in = petSrv.getPetFull(pet.get("petID")).getIncomingLikes();
						
						if(in == null)
							in = new ArrayList<String>();
						
						for(int j = 0; j < in.size(); j++){
							temp.put("myPetID", tempPetID);
							temp.put("otherPetID", in.get(j));
							listArray.add(temp);
						}
					}

					responseObj.put("list", listArray);
					responseObj.put("response", 1);
					System.out.println("Get Incoming Likes successful.");
				}
				else {
					responseObj.put("response", -1);
					System.out.println("Pet does not exist.");
				}
			}
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Get Incoming Likes Failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
	@POST
	@Path("/getOutgoingLikes")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getIncomingMatches(Map<String, String> pet) throws Exception{
		Map<String, Object> responseObj = new HashMap<String, Object>();
		Response.ResponseBuilder builder = null;
		System.out.println("----- Get Outgoing Likes Request ----- ");
		try {
			if(tknSrv.checkToken(pet.get("token")).equals(TokenStatus.SUCCESS)){
				if(petSrv.petExists(pet.get("petID"))){
					String eMail = usrSrv.getEmailFromToken(pet.get("token"));
					ArrayList<String> list = usrSrv.getUserPets(eMail);
					
					if(list == null)
						list = new ArrayList<String>();
					
					ArrayList<HashMap<String, Object>> listArray = new ArrayList<HashMap<String, Object>>();
								
					for(int i = 0; i < list.size(); i++) {
						HashMap<String, Object> temp = new HashMap<String, Object>();
						String tempPetID = list.get(i);
						
						ArrayList<String> out = petSrv.getPetFull(pet.get("petID")).getOutgoingLikes();
						
						if(out == null)
							out = new ArrayList<String>();
						
						for(int j = 0; j < out.size(); j++){
							temp.put("myPetID", tempPetID);
							temp.put("otherPetID", out.get(j));
							listArray.add(temp);
						}
					}

					responseObj.put("list", listArray);
					responseObj.put("response", 1);
					System.out.println("Get Incoming Likes successful.");
				}
				else {
					responseObj.put("response", -1);
					System.out.println("Pet does not exist.");
				}
			}
			else {
				responseObj.put("response", -2);
				System.out.println("Unknown user detected.");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			responseObj.put("response", 0);
			System.out.println("Get Outgoing Likes Failed.");
			throw e;
		}
		
		builder = Response.status(Response.Status.OK).entity(responseObj);
		return builder.build();
	}
	
}