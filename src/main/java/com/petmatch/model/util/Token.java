package com.petmatch.model.util;


import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Reference;

import com.petmatch.model.core.User;
import com.petmatch.model.definition.BaseEntity;

@Entity("tokens")
public class Token extends BaseEntity {
	
	private String tokenKey;
	private String userEmail;

	public String getTokenKey() {
		return tokenKey;
	}
	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
}