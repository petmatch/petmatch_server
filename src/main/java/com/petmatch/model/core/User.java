package com.petmatch.model.core;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.IndexOptions;
import org.mongodb.morphia.annotations.Indexed;

import com.petmatch.model.definition.BaseEntity;

@XmlRootElement
@Entity("users")
public class User extends BaseEntity{
	private String username;
	private String ownerName;
	private String ownerSurname;
	@Indexed(options=@IndexOptions(unique=true))
	private String eMail;
	private String password;
	private String phone;
	private String city;
	private int ownerGender;
	private boolean isPremium;
	private ArrayList<String> petIDList;
	private ArrayList<String> myAdopts;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOwnerSurname() {
		return ownerSurname;
	}
	public void setOwnerSurname(String ownerSurname) {
		this.ownerSurname = ownerSurname;
	}
	public String getEmail() {
		return eMail;
	}	
	public void setEmail(String email) {
		this.eMail = email;
	}	
	public String getPassword() {
		return password;
	}	
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getOwnerGender() {
		return ownerGender;
	}
	public void setOwnerGender(int ownerGender) {
		this.ownerGender = ownerGender;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public boolean isPremium() {
		return isPremium;
	}
	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}
	public ArrayList<String> getPetIDList() {
		return petIDList;
	}
	public void setPetIDList(ArrayList<String> petIDList) {
		this.petIDList = petIDList;
	}
	public ArrayList<String> getMyAdopts() {
		return myAdopts;
	}
	public void setMyAdopts(ArrayList<String> myAdopts) {
		this.myAdopts = myAdopts;
	}
}