package com.petmatch.model.core;

public class Vaccine {
	private String vaccineID;
	private String vaccineName;
	private String vaccineDate;


	public String getVaccineID() {
		return vaccineID;
	}
	public void setVaccineID(String vaccineID) {
		this.vaccineID = vaccineID;
	}
	public String getVaccineName() {
		return vaccineName;
	}
	public void setVaccineName(String vaccineName) {
		this.vaccineName = vaccineName;
	}
	public String getVaccineDate() {
		return vaccineDate;
	}
	public void setVaccineDate(String vaccineDate) {
		this.vaccineDate = vaccineDate;
	}
}
