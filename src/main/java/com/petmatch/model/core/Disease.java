package com.petmatch.model.core;

public class Disease {
	private String diseaseID;
	private String diseaseName;
	private boolean isCurrent;

	public String getDiseaseID() {
		return diseaseID;
	}
	public void setDiseaseID(String diseaseID) {
		this.diseaseID = diseaseID;
	}
	public String getDiseaseName() {
		return diseaseName;
	}
	public void setDiseaseName(String diseaseName) {
		this.diseaseName = diseaseName;
	}
	public boolean isCurrent() {
		return isCurrent;
	}
	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
}
