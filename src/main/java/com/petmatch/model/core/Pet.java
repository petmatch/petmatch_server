package com.petmatch.model.core;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Reference;

import com.petmatch.model.definition.BaseEntity;

@XmlRootElement
@Entity("pets")
public class Pet extends BaseEntity{
	private String ownerEmail;
	private String petName;
	private String petType;
	private int petGender;
	private int petAge;
	private boolean isSterilized;
	private String petCity;
	private boolean pendingMatch;
	private boolean pendingAdoption;
	private ArrayList<String> prefType;
	private String petProfilePhotoPath;
	private String vetMail;
	private String vetPhone;
	private ArrayList<String> incomingLikes;
	private ArrayList<String> outgoingLikes;

	public String getOwnerEmail() {
		return ownerEmail;
	}
	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}
	public String getPetName() {
		return petName;
	}
	public void setPetName(String petName) {
		this.petName = petName;
	}
	public String getPetType() {
		return petType;
	}
	public void setPetType(String petType) {
		this.petType = petType;
	}
	public int getPetGender() {
		return petGender;
	}
	public void setPetGender(int petGender) {
		this.petGender = petGender;
	}
	public int getPetAge() {
		return petAge;
	}
	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}
	public boolean isSterilized() {
		return isSterilized;
	}
	public void setSterilized(boolean isSterilized) {
		this.isSterilized = isSterilized;
	}
	public String getPetCity() {
		return petCity;
	}
	public void setPetCity(String petCity) {
		this.petCity = petCity;
	}
	public boolean isPendingMatch() {
		return pendingMatch;
	}
	public void setPendingMatch(boolean pendingMatch) {
		this.pendingMatch = pendingMatch;
	}
	public boolean isPendingAdoption() {
		return pendingAdoption;
	}
	public void setPendingAdoption(boolean pendingAdoption) {
		this.pendingAdoption = pendingAdoption;
	}
	public ArrayList<String> getPrefType() {
		return prefType;
	}
	public void setPrefType(ArrayList<String> prefType) {
		this.prefType = prefType;
	}	
	public String getPetProfilePhotoPath() {
		return petProfilePhotoPath;
	}
	public void setPetProfilePhotoPath(String petProfilePhotoPath) {
		this.petProfilePhotoPath = petProfilePhotoPath;
	}
	public String getVetMail() {
		return vetMail;
	}
	public void setVetMail(String vetMail) {
		this.vetMail = vetMail;
	}
	public String getVetPhone() {
		return vetPhone;
	}
	public void setVetPhone(String vetPhone) {
		this.vetPhone = vetPhone;
	}
	public ArrayList<String> getIncomingLikes() {
		return incomingLikes;
	}
	public void setIncomingLikes(ArrayList<String> incomingLikes) {
		this.incomingLikes = incomingLikes;
	}
	public ArrayList<String> getOutgoingLikes() {
		return outgoingLikes;
	}
	public void setOutgoingLikes(ArrayList<String> outgoingLikes) {
		this.outgoingLikes = outgoingLikes;
	}
}