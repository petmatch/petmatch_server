package com.petmatch.model.core;

public class Veterinarian {
	private String vetID;
	private String vetName;
	private String vetPhone;
	private String vetAddress;
	
	public String getVetID() {
		return vetID;
	}
	public void setVetID(String vetID) {
		this.vetID = vetID;
	}
	public String getVetName() {
		return vetName;
	}
	public void setVetName(String vetName) {
		this.vetName = vetName;
	}
	public String getVetPhone() {
		return vetPhone;
	}
	public void setVetPhone(String vetPhone) {
		this.vetPhone = vetPhone;
	}
	public String getVetAddress() {
		return vetAddress;
	}
	public void setVetAddress(String vetAddress) {
		this.vetAddress = vetAddress;
	}
}
