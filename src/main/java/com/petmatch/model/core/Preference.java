package com.petmatch.model.core;

import java.util.ArrayList;

public class Preference {
	//private int minPetAge;
	//private int maxPetAge;
	
	private String prefGender;
	private String prefCity;
	private ArrayList<String> prefPetType;
	
	/*
	public int getMinPetAge() {
		return minPetAge;
	}
	public void setMinPetAge(int minPetAge) {
		this.minPetAge = minPetAge;
	}
	public int getMaxPetAge() {
		return maxPetAge;
	}
	public void setMaxPetAge(int maxPetAge) {
		this.maxPetAge = maxPetAge;
	}*/
	
	public String getPrefGender() {
		return prefGender;
	}
	public void setPrefGender(String prefGender) {
		this.prefGender = prefGender;
	}
	public String getPrefCity() {
		return prefCity;
	}
	public void setPrefCity(String prefCity) {
		this.prefCity = prefCity;
	}
	public ArrayList<String> getPrefPetType() {
		return prefPetType;
	}
	public void setPrefPetType(ArrayList<String> prefPetType) {
		this.prefPetType = prefPetType;
	}
}
